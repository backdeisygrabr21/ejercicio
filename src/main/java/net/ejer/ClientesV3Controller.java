package net.ejer;


import net.ejer.data.ClienteMongo;
import net.ejer.data.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


/*Con esta version vamos a conectarnos con MongoDB*/
//@SpringBootApplication
public class ClientesV3Controller implements CommandLineRunner {

    @Autowired
    private ClienteRepository repository;

   public static void main(String[] args) {

       SpringApplication.run(ClientesV3Controller.class, args);
   }

    @Override
    public void run(String... args) throws Exception {
       System.out.println("Preparando MongoDB");
       repository.deleteAll();
       repository.insert(new ClienteMongo("Carolina","Mendez","Bogota"));
       repository.insert(new ClienteMongo("Angela","Perez","Chìa"));
       repository.insert(new ClienteMongo("Camilo","Martinez","Cali"));
       List<ClienteMongo> lista = repository.findAll();
       for (ClienteMongo p:lista){
           System.out.println(p.toString());
        }
    }
}
