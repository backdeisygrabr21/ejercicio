package net.ejer;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/*Con esta version se usa una lista de clientes no se utiliza la clase cliente*/
//@RestController
public class ClientesV2Controller {

    private ArrayList<Cliente>listaClientes = null;

    public ClientesV2Controller(){
        listaClientes = new ArrayList<>();
        listaClientes.add(new Cliente(1,"Diana", "Gonzalez","Cartagena"));
        listaClientes.add(new Cliente(2,"Maria", "Perez","Bogotá"));
        listaClientes.add(new Cliente(1,"Edwin", "Montaño","Cali"));
    }
    /*Consulta la lista de clientes */
    @GetMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<List<Cliente>> obtenerListado()

    {
        return new ResponseEntity<>(listaClientes, HttpStatus.OK);
    }

    /*Consulta un cliente especifico*/
    @GetMapping(value = "/clientes/{id}", produces = "application/json")
    public ResponseEntity<Cliente> obtenerClienteById(@PathVariable int id)
    {
        Cliente resultado = null;
        ResponseEntity<Cliente> respuesta = null;
        try{
            resultado = listaClientes.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
           // resultado = "No se ha encontrado el cliente";
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }


    /*Crea un nuevo cliente */
    @PostMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<String>addcliente(@RequestBody Cliente clienteNuevo){
        //listaClientes.add("Otro cliente");
        listaClientes.add(clienteNuevo);
        return new ResponseEntity<>("Cliente creado correctamente", HttpStatus.CREATED);
    }

    /*Crea un cliente enviando los campos en la URL*/
    @PostMapping(value = "/clientes/{nom}/{ape}/{ciu}", produces = "application/json")
    public void AddCliente(@PathVariable("nom") String nombre, @PathVariable("ape") String apellido, @PathVariable("ciu") String ciudad){
        listaClientes.add(new Cliente(12,"Nuevo", "Cliente", "URL"));
    }

    /*Modifica un cliente*/
    @PutMapping("/clientes/{id}")
    public ResponseEntity<String> updateCliente(@PathVariable int id, @RequestBody Cliente cambios)
    {
        ResponseEntity<String> resultado = null;
        try {
            Cliente clienteAModificar = listaClientes.get(id);
            clienteAModificar.setNombre(cambios.getNombre());
            clienteAModificar.setApellido(cambios.getApellido());
            clienteAModificar.setCiudad(cambios.getCiudad());
            listaClientes.set(id, clienteAModificar);
            resultado = new ResponseEntity<>("Cliente actualizado", HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el cliente",HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /*Borra cliente por ID*/
    @DeleteMapping("/clientes/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try {
            Cliente clienteAEliminar = listaClientes.get(id-1);
            listaClientes.remove(id-1);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el cliente",HttpStatus.NO_CONTENT);
        }
        return resultado;
    }

    /*Borra todos los clientes*/
    @DeleteMapping("/clientes")
    public ResponseEntity<String> deleteCliente()
    {
        ResponseEntity<String> resultado = null;
        listaClientes.clear();
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }
}
