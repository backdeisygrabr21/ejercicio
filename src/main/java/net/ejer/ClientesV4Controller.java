package net.ejer;

import net.ejer.data.ClienteMongo;
import net.ejer.data.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ClientesV4Controller {
    @Autowired
    private ClienteRepository repository;

    /*Consulta la lista de clientes */
    @GetMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<List<ClienteMongo>> obtenerListado()
    {
        List<ClienteMongo> lista = repository.findAll();
        return new ResponseEntity<>(lista,HttpStatus.OK);
    }

    /*Obtiene un cliente por el nombre */
    @GetMapping(value = "/clientesbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ClienteMongo>> obtenerClientePorNombre(@PathVariable String nombre)
    {
        List<ClienteMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<ClienteMongo>>(resultado, HttpStatus.OK);
    }


    /* Crea un cliente */
    @PostMapping(value="/clientes")
    public ResponseEntity<String>addCliente(@RequestBody ClienteMongo clienteMongo){
        ClienteMongo resutado = repository.insert(clienteMongo);
        return new ResponseEntity<String>(resutado.toString(),HttpStatus.OK);
    }

    /*Actualiza un Cliente */
    @PutMapping(value = "/clientes/{id}")
    public ResponseEntity<String> updateCliente(@PathVariable String id, @RequestBody ClienteMongo clienteMongo)
    {
        Optional <ClienteMongo> resultado = repository.findById(id);
        if (resultado.isPresent()){
            ClienteMongo productoAModificar = resultado.get();
            productoAModificar.nombre = clienteMongo.nombre;
            productoAModificar.apellido = clienteMongo.apellido;
            productoAModificar.ciudad = clienteMongo.ciudad;
            ClienteMongo guardado = repository.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
        }
        return new ResponseEntity<String>("Prducto no encontrado",HttpStatus.NOT_FOUND);
    }

    /*Borra un cliente */
    @DeleteMapping(value="/clientes/{id}")
    public ResponseEntity<String>deleteCliente(@PathVariable String id)
    {
        /*Se puede primero buscar el cliente y si existe eliminarlo */
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto borrado",HttpStatus.OK);
    }
}
