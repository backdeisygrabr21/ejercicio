package net.ejer.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends MongoRepository<ClienteMongo, String> {
    @Query("{'nombre':?0}")
    public List<ClienteMongo> findByNombre(String nombre);
}



