package net.ejer;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/*Con esta version se usa una lista de clientes no se utiliza la clase cliente*/
//@RestController
public class ClientesController {

    private ArrayList<String>listaClientes = null;

    public ClientesController(){
        listaClientes = new ArrayList<>();
        listaClientes.add("Diana");
        listaClientes.add("Carlos");
    }
    /*Consulta la lista de clientes */
    @GetMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<List<String>> obtenerListado()
    {
        return new ResponseEntity<>(listaClientes, HttpStatus.OK);
    }

    /*Consulta un cliente especifico*/
    @GetMapping(value = "/clientes/{id}", produces = "application/json")
    public ResponseEntity<String> obtenerClienteById(@PathVariable int id)
    {
        String resultado = null;
        ResponseEntity<String> respuesta = null;
        try{
            resultado = listaClientes.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            resultado = "No se ha encontrado el cliente";
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }


    /*Crea un nuevo cliente */
    @PostMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<String>addcliente(){
        listaClientes.add("Otro cliente");
        return new ResponseEntity<>("Cliente creado correctamente", HttpStatus.CREATED);
    }

    /*Crea un cliente enviando los campos en la URL*/
    @PostMapping(value = "/clientes/{nom}/{ape}", produces = "application/json")
    public void AddCliente(@PathVariable("nom") String nombre, @PathVariable("ape") String apellido){
        listaClientes.add(nombre);
    }

    /*Modifica un cliente*/
    @PutMapping("/clientes/{id}")
    public ResponseEntity<String> updateCliente(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try {
            String clienteAModificar = listaClientes.get(id);
            resultado = new ResponseEntity<>("Cliente actualizado", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el cliente",HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /*Borra cliente*/
    @DeleteMapping("/clientes/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try {
            String clienteAEliminar = listaClientes.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return resultado;
    }
}
